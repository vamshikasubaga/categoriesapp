import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;

public class CategoriesMain {

	public static void main(String[] args) {
		// Checking if correct number of arguments were supplied
		if (args.length != 1) {
			System.err.println("No input file was provided. Please provide file name in arguments");
			return;
		}

		// Reading the file and handling the exception for improper data
		BufferedReader reader = null;
		try { 
			reader = new BufferedReader(new FileReader(args[0]));
		} catch (IOException e) {
			System.err.println(e.getMessage());
			return;
		}

		// Using HashMap to keep count of each category in the input file 
		HashMap<String, Integer> categoryCount = new HashMap<String, Integer>();

		// StringBuilder is used to output the category input in sequence.
		StringBuilder sb = new StringBuilder();

		// HashSet to keep a list of valid categories that provide constant time
		// lookup
		categoryCount.put("PERSON", 0);
		categoryCount.put("PLACE", 0);
		categoryCount.put("ANIMAL", 0);
		categoryCount.put("COMPUTER", 0);
		categoryCount.put("OTHER", 0);

		// String variable to read the file line-by-line
		String line = null;
		try {
			// Loop till end of file
			while ((line = reader.readLine()) != null) {

				// Skip empty lines
				if (line.isEmpty())
					continue;

				// Fetch the category from the input line
				int categoryEnd = line.indexOf(" ");
				String category = line.substring(0, categoryEnd).toUpperCase();

				// Check if the category read is valid and we are not looking at
				// duplicate values
				if (categoryCount.containsKey(category)
						&& !sb.toString().contains(line)) {
					// Append the line to the output string builder
					sb.append(line);
					sb.append("\n\n");

					// Update the category counter
					categoryCount.put(category, categoryCount.get(category) + 1);
				}
			}

			// Output Formatting 
			System.out.println("CATEGORY \t COUNT\n");
			System.out.println("PERSON" + " \t " + categoryCount.get("PERSON") + "\n");
			System.out.println("PLACE" + " \t " + categoryCount.get("PLACE") + "\n");
			System.out.println("ANIMAL" + " \t " + categoryCount.get("ANIMAL") + "\n");
			System.out.println("COMPUTER" + " \t " + categoryCount.get("COMPUTER") + "\n");
			System.out.println("OTHER" + " \t " + categoryCount.get("OTHER") + "\n");
			System.out.println(sb.toString());
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} finally {
			// Close the reader if it is open
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e) {
					System.err.println(e.getMessage());
				}
			}
		}
	}

}